cmake_minimum_required(VERSION 3.0.0)
project(Ravesli VERSION 0.1.0)

include(CTest)
enable_testing()

set(PROJECT_SOURCES
        Ravesli.cpp
        Ravesli.h
        sortfuncs.cpp
        sortfuncs.h
        additionalfuncs.cpp
        additionalfuncs.h
)

add_executable(Ravesli ${PROJECT_SOURCES})

set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})

include(CPack)
