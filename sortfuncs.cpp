#include "sortfuncs.h"

// Type alias
using validateFnc = bool (*)(int, int);
// std::function<bool(int,int)> func;
// Typedef bool (*validateFnc)(int, int)

// Пирамидальная сортировка
void hrapSort(int* array, int length) {}

/*
Для слияния мы создаём новый массив, изначально пустой.
Затем мы параллельно идём по двум частям изначального массива, поддерживая два
указателя, и на каждой итерации добавляем в новый массив меньший элемент из двух
текущих, сдвигая вперёд соответствующий указатель. После добавления всех
элементов из двух сливаемых частей содержание нового массива копируется в
изначальный массив
*/
// Сортировка слиянием
void mergeSort(int* array, int length, validateFnc comparisonFcn) {}

void newArrayGeneration(int* newArray, int* oldArray, int lengthOldArray,
                        bool isLast, int tempLengthSecondArray = 0) {
    if (isLast) {
        for (size_t i = 0; i < lengthOldArray; i++) {
            newArray[i + tempLengthSecondArray] = oldArray[i];
        }
    } else {
        for (size_t i = 0; i < lengthOldArray; i++) {
            newArray[i] = oldArray[i + tempLengthSecondArray];
        }
    }
}

void quickSortSupportingValue(int* array, int mainValue, int relocatableValue,
                              int length) {
    // Когда элемент главный больше передвигаемого
    if (array[mainValue] > array[relocatableValue]) {
        // Индекс главного больше передвигаемого
        if (mainValue > relocatableValue) {
            quickSortSupportingValue(array, mainValue, ++relocatableValue,
                                     length);
        } else {  // Если индекс главного меньше передвигаемого
            std::swap(array[mainValue], array[relocatableValue]);
            quickSortSupportingValue(array, relocatableValue, ++mainValue,
                                     length);
        }
    } else if (array[mainValue] < array[relocatableValue]) {
        if (mainValue > relocatableValue) {
            std::swap(array[mainValue], array[relocatableValue]);
            quickSortSupportingValue(array, relocatableValue, --mainValue,
                                     length);
        } else {
            quickSortSupportingValue(array, mainValue, --relocatableValue,
                                     length);
        }
    } else if (mainValue == relocatableValue && mainValue >= 0 && length != 0)
    /*
    if ((mainValue > relocatableValue &&
                array[mainValue] > array[relocatableValue]) ||
               (mainValue < relocatableValue &&
                array[mainValue] < array[relocatableValue]))
    */
    // return;
    // else
    {
        /*
        Тут нужно два массива, после обработки которых нужно соединить
        воедино. Рекурсия сама соединит все массивы по завершению
        сортировки. Первый массив до главного элемента, второй остаётся
        прежним так как там находится главный элемент на данный момент,
        третий состоит из элементов, которые идут после главного. Поделить
        массив - пройтись по элементам до главного и добавить их в новый
        массив. Для начала надо получить длину до главного элемента массива
        не включая его - для первого массива и после главного до конца - для
        второго массива.
        */

        int lengthFirstArray = mainValue;
        auto* firstArray{new (std::nothrow) int[lengthFirstArray]{}};
        newArrayGeneration(firstArray, array, lengthFirstArray, false);
        quickSortSupportingValue(firstArray, 0, lengthFirstArray - 1,
                                 lengthFirstArray);

        int lengthSecondArray = length - mainValue - 1;
        auto* secondArray{new (std::nothrow) int[lengthSecondArray]{}};
        newArrayGeneration(secondArray, array, lengthSecondArray, false,
                           lengthFirstArray + 1);
        quickSortSupportingValue(secondArray, 0, lengthSecondArray - 1,
                                 lengthSecondArray);

        newArrayGeneration(array, firstArray, lengthFirstArray, true);
        newArrayGeneration(array, secondArray, lengthSecondArray, true,
                           lengthFirstArray + 1);

        // delete[] firstArray;
        // firstArray = nullptr;
        // delete[] secondArray;
        // secondArray = nullptr;
        // return;
    }
}

// Быстрая сортировка
void quickSort(int* array, int length, validateFnc comparisonFcn) {
    /*
    relocatableValue = length - 1;
    */
    int mainValue = 0;
    int relocatableValue = length - 1;
    quickSortSupportingValue(array, mainValue, relocatableValue, length);
}

/*
!Если правильный порядок, то он шагает дальше, иначе он
меняет их местами и шагает назад. Условия: если нет предыдущего элемента, он
шагает вперед; если нет следующего, то он закончил.
*/
// Гномья сортировка без указателей
void gnomeSort(int* array, int length, validateFnc comparisonFcn) {
    int step = 1;
    while (step != length) {
        if (step != 0 && comparisonFcn(array[step - 1], array[step])) {
            std::swap(array[step - 1], array[step]);
            step--;
        } else
            step++;
    }
}

// Сортировка Шелла без рекурсии
void shellSort(int* array, int length, validateFnc comparisonFcn) {
    int delta = 2;
    int step = length / delta;

    while (step >= 1) {
        for (size_t i = 0; i < length - step; i++) {
            if (comparisonFcn(array[i], array[i + step])) {
                std::swap(array[i], array[i + step]);
            }
        }
        delta++;
        step = length / delta;
    }
}

// Сортировка вставками
void insertSort(int* array, int length, validateFnc comparisonFcn) {
    for (int i = 0; i < length; i++) {
        for (int k = i; k > 0; k--) {
            if (comparisonFcn(array[k - 1], array[k])) {
                std::swap(array[k], array[k - 1]);
            } else {
                break;
            }
        }
    }
}

// Сортировка расчёсткой
void combSort(int* array, int length, validateFnc comparisonFcn) {
    const double factor = 1.247;
    int step = length - 1;

    while (step >= 1) {
        for (int i = 0; i < length - step; i++) {
            if (comparisonFcn(array[i], array[i + step])) {
                std::swap(array[i], array[i + step]);
            }
        }

        step /= factor;
    }
}

// Шейкерная сортировка
void coctailSort(int* array, int length, validateFnc comparisonFcn) {
    int left = 0;
    int right = length - 1;

    while (left < right) {
        for (int i = left; i < right; i++) {
            if (comparisonFcn(array[i], array[i + 1])) {
                std::swap(array[i], array[i + 1]);
            }
        }

        right--;

        for (int i = right; i > left; i--) {
            if (comparisonFcn(array[i - 1], array[i])) {
                std::swap(array[i], array[i - 1]);
            }
        }

        left++;
    }
}

// Пузырьком
void bubbleSort(int* array, int length, validateFnc comparisonFcn) {
    for (int i = 0; i < length; ++i) {
        for (int k = 0; k < length - i - 1; ++k) {
            if (comparisonFcn(array[k], array[k + 1])) {
                std::swap(array[k], array[k + 1]);
            }
        }
    }
}

// Сортировка выбором
void selectionSort(int* array, int length, validateFnc comparisonFcn) {
    for (int i = 0; i < length; i++) {
        for (int k = i; k < length; k++) {
            if (comparisonFcn(array[i], array[k])) {
                std::swap(array[i], array[k]);
            }
        }
    }
}
